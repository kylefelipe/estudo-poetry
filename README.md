# Poetry

Para usar o poetry é interessante ter algo que ajude a gerênciar a versão do [Python](www.python.org). No momento de criar o ambiente, o [Poetry](https://python-poetry.org/) irá utilizar o [Python](www.python.org) que estiver "padrão" no momento da ativação dele.

## Pipenv

O [Pyenv](https://github.com/pyenv) é um gerenciador de versões do [Python](www.python.org).
Com ele instalado é possível instalar as versões do [Python](www.python.org) que desejarmos e criar os ambientes virtuais, isolar o [Python](www.python.org) padrão do sistema (em ambientes Linux e Mac que utilizam o [Python](www.python.org)) e configurar a versão a ser utilizada.

## Instalação do Pyenv

Para instalar, basta seguir as instruções do [repositório](https://github.com/pyenv/pyenv-installer).

## Preparando o ambiente no Pyenv

Instale as versões do Python no [Pyenv](https://github.com/pyenv):

```Shell
pyenv install python-3.8.0 #Irá instalar a versão 3.8.0
```

Para setar a versão do [Python](www.python.org) a ser usado no momento em que for criado o ambiente, antes, é necessário colocar o python instalado no [Pyenv](https://github.com/pyenv) como padrão:

```Shell
pyenv local <versão do python> # Ex.: 3.8.0
```

Isso ira colocar o [Python](www.python.org) como padrão localmente.

```Shell
python --version
# Python 3.8.0
```

Para usar alguma versão como global:

```Shell
pyenv global <versão do python> # Ex.: 3.8.0
```

## Fazer o pyenv funcionar no Fish

Caso utilize o [Fish Shell](https://fishshell.com/)

Para fazer o [Pyenv](https://github.com/pyenv) funcionar no fish edite o arquivo de configuração do fish em `~/.config/fish/config.fish` E adicione as linhas a seguir nele.

[Dica do sirkonst](https://gist.github.com/sirkonst/e39bc28218b57cc78b6f728b8da99f33)

```Shell
set PYENV_ROOT $HOME/.pyenv
set -x PATH $PYENV_ROOT/bin $PATH
status --is-interactive; and . (pyenv init -|psub)
status --is-interactive; and . (pyenv virtualenv-init -|psub)
set -x VIRTUAL_ENV_DISABLE_PROMPT 1
```

## Usando o Poetry

Começando um novo projeto:

```Shell
poetry new <nome do projeto>
```

Ele irá pegar o python setado como padrão (como feito no Preparando o ambiente no [Pyenv](https://github.com/pyenv)) e utilizar o novo projeto, e criar uma estrutura de pastas padrão.

Instalando dependências para o projeto:

```Shell
poetry add <pacote>

# Para pacotes que não são estáveis

poetry add <pacote> --allow-prereleases

# Para pacotes de desenvolvimento, como linters, formatadores e etc.

poetry add -D <pacote>
```

Na primeira vez (logo após a instalação), tive problema ao fazer o `poetry add <pacote>`, aparecia sempre o erro `No module named pip`, eu iniciei o projeto usando o seguinte comando:

```Shell
poetry init
```

Esse comando inicia o poetry iterativamente na pasta que estiver

E fui seguindo as instruções na tela, depois disso eu consegui usar o `poetry add` normalmente em qualquer outro projeto.

## Desenvolvendo

Essas dicas eu peguei dos tutoriais do [Python Cheatssheet](https://www.pythoncheatsheet.org/)

[Tutorial 1](https://www.pythoncheatsheet.org/blog/python-projects-with-poetry-and-vscode-part-1/)
[Tutorial 2](https://www.pythoncheatsheet.org/blog/python-projects-with-poetry-and-vscode-part-2/)
[Tutorial 3](https://www.pythoncheatsheet.org/blog/python-projects-with-poetry-and-vscode-part-3/)

Entre na pasta em que criou o projeto e inicie o shell do poetry

```Shell
poetry shell
```

Ele irá criar um virtualenv para o projeto e irá iniciar o shell.

```Shell
code .
```

Irá iniciar o VScode com ambiente já configurado para o projeto.

Para sair do shell do Poetry:

```Shell
exit
```

## Unit Tests no VSCODE

De acordo com a [documentação](https://code.visualstudio.com/docs/python/testing) do VsCode, os testes são desabilitados, por padrão.  
Para habilitar:

`CTRL +p` e procure por "Configure Tests" e escolha o pacote de testes desejado e a pasta onde estão instalados os testes.
